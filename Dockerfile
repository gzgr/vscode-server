FROM gitpod/openvscode-server:latest

USER root

RUN apt-get update && DEBIAN_FRONTEND=noninteractive TZ=Asia/Seoul apt-get install -my curl wget make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

# Go
#RUN wget https://golang.org/dl/go1.17.7.linux-amd64.tar.gz
#RUN tar xvf go1.17.7.linux-amd64.tar.gz -C /home/
#RUN chown -R openvscode-server:openvscode-server /home/go

# User
USER openvscode-server

RUN curl https://pyenv.run | bash
ENV PATH $HOME/.pyenv/bin:$PATH
ENV eval "$(pyenv init --path)"
ENV eval "$(pyenv virtualenv-init -)"
ENV eval "$(pyenv init -)"

RUN pyenv install 3.10.2

ENV PATH /home/go/bin:$PATH
ENV GOPATH /home/workspace/go
ENV PATH $GOPATH/bin:$PATH
